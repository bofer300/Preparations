# Docker Workshop
Preparations

---

## Run the command below:

Bash:

```
docker pull selaworkshops/jenkins-blueocean && \
docker pull selaworkshops/npm-static-app:latest && \
docker pull selaworkshops/npm-static-app:3.0 && \
docker pull selaworkshops/alpine:3.4 && \
docker pull selaworkshops/busybox:latest && \
docker pull selaworkshops/python-app:2.0 && \
docker pull selaworkshops/ubuntu:18.04 && \
docker pull selaworkshops/ngnix:alpine && \
docker pull docker.bintray.io/jfrog/artifactory-oss:latest && \
docker pull grafana/grafana && \
docker pull graphiteapp/graphite-statsd
```

Powershell:

```
docker pull selaworkshops/jenkins-blueocean ; 
docker pull selaworkshops/npm-static-app:latest ; 
docker pull selaworkshops/npm-static-app:3.0 ; 
docker pull selaworkshops/alpine:3.4 ; 
docker pull selaworkshops/busybox:latest ; 
docker pull selaworkshops/python-app:2.0 ;
docker pull selaworkshops/ubuntu:18.04 ; 
docker pull selaworkshops/ngnix:alpine ; 
docker pull docker.bintray.io/jfrog/artifactory-oss:latest ; 
docker pull grafana/grafana ; 
docker pull graphiteapp/graphite-statsd
```
